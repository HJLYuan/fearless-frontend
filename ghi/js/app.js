function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card shadow-sm p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <small class ="text-muted">${starts} - ${ends}</small>
        </div>
      </div>
    `;
  }

// function formatDate(dateInfo) {
//     let timeHours = parseInt(dateInfo.substring(11, 13));
//     let timeMinutes = dateInfo.substring(14, 16);
//     let tiemPostscript = "AM";
//     let month = dateInfo.substring(5,7);

// }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        throw new Error('Response not ok');
    } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts);
            const starts = start.toLocaleDateString();
            const end = new Date(details.conference.ends);
            const ends = end.toLocaleDateString();
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
            
          }
        }
  
      }
    } catch (e) {
      console.error("error");
    }
  
  });