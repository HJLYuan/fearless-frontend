window.addEventListener('DOMContentLoaded', async () => {
    
    const url = 'http://localhost:8000/api/conferences/';
    
    const response = await fetch(url);


    
    
    if (response.ok) {
        const data = await response.json();
        


      const selectTag = document.getElementById('conferences');
      for (let conference of data.conferences) {
        let option = document.createElement("option");
        option.value = location.href;
        option.innerHTML = conference;
        selectTag.appendChild(option);

        
      }
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        
        
        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        let res = await fetch(locationUrl, fetchConfig);
        if (res.ok) {
            formTag.reset();
            const newLocation = await res.json();
        }
    });         
  });